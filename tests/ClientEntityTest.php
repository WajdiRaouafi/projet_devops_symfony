<?php 
namespace gitlab\tests;
use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ClientEntityTest extends KernelTestCase {   

    public function testValifEntity() {
        $entity =( new Client())
            ->setNom("Med")
            ->setPrenon("Raouafi")
            ->setCin("34")
            ->setAdresse("add 1 test");
        self::bootKernel();
        $validator= self::$container->get('validator');
        $error = $validator->validate($entity);
        $this->assertCount(0, $error);
    }
}
