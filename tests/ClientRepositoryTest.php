<?php
namespace gitlab\tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Client;

class ClientRepositoryTest extends WebTestCase  {
    
    public function testFindAllClients() {
        $client = static::createClient();

        $entityManager = $client->getContainer()->get('doctrine')->getManager();
        $clientEntity = (new Client())
        ->setNom('John')
        ->setPrenon('Doe')
        ->setCin(123456)
        ->setAdresse('Test Address');
        $entityManager->persist($clientEntity);
        $entityManager->flush();

        $clientRepository = $entityManager->getRepository(Client::class);
        $clients = $clientRepository->findAll();
        $this->assertNotEmpty($clients);
        $this->assertCount(1, $clients);

        $persistedClient = $clients[0];
        $this->assertInstanceOf(Client::class, $persistedClient);
        $this->assertSame('John', $persistedClient->getNom());
        $this->assertSame('Doe', $persistedClient->getPrenon());
        $this->assertSame(123456, $persistedClient->getCin());
        $this->assertSame('Test Address', $persistedClient->getAdresse());

        $entityManager->remove($persistedClient);
        $entityManager->flush();

    }
}